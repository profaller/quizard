<?php
namespace app\components;

use yii\base\Behavior;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;


class UploadFilesBehavior extends Behavior
{
    public $attributes;

    public $uploadPath;

    public $uploadUrl;

    public $identity = 'id';



    public function instantiateFile()
    {
        foreach ($this->attributes as $attribute) {
            $uploadFile = UploadedFile::getInstance($this->owner, $attribute);
            if ($uploadFile) {
                $this->owner->$attribute = $uploadFile;
            } else {
                $this->owner->$attribute = $this->owner->oldAttributes[$attribute];
            }
        }
    }

    public function _upload($removeOld = true)
    {
        $this->upload($removeOld);
    }

    public function upload($removeOld = true)
    {
        $this->instantiateFile();

        foreach ($this->attributes as $attribute) {

            if ($this->owner->$attribute instanceof UploadedFile && $this->owner->$attribute->error == 0) {

                if ($removeOld) {
                    FileHelper::removeDirectory($this->getUploadPath($attribute));
                }

                if (!file_exists($this->getUploadPath($attribute))) {
                    FileHelper::createDirectory($this->getUploadPath($attribute));
                }

                $this->uploadSingle($this->owner->$attribute, $attribute);
            }
        }

    }

    /**
     * @param UploadedFile $file
     * @param string $attribute
     */
    protected function uploadSingle($file, $attribute)
    {
        if ($file && $file->error == 0) {
            $fileName = $this->generateName($file);
            $filePath = $this->getUploadPath($attribute) . $fileName;

            if (!file_exists($filePath)) {
                $file->saveAs($filePath);
            }

            $this->owner->{$attribute} = $fileName;
            $this->owner->save(false, [$attribute]);
        }
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    protected function generateName($file)
    {
        return md5_file($file->tempName) . md5(microtime()) . '.' . $file->extension;
    }

    public function getUploadPath($attribute, $includeFilename = false)
    {
        return $this->uploadPath . $this->owner->{$this->identity} . DIRECTORY_SEPARATOR . $attribute . DIRECTORY_SEPARATOR . ($includeFilename ? $this->owner->{$attribute} : '');
    }

    public function getUploadUrl($attribute)
    {
        return $this->uploadUrl . $this->owner->{$this->identity} . '/' . $attribute . DIRECTORY_SEPARATOR . $this->owner->{$attribute};
    }
}