<?php


namespace app\components;

use app\models\Landings;
use app\services\FBPixelService;
use yii\helpers\Url;
use yii\twig\ViewRenderer;
use \Yii;

class LandingRenderer extends ViewRenderer
{
    /**
     * @var Landings
     */
    public $landing;


    public function init()
    {
        $localeCode = $this->landing->locale->id;

        $this->functions = [
            'dbg' => 'dbg',

            new \Twig_SimpleFunction('fb_pixel', function () {

                return FBPixelService::create()->getFBPixelAction();

            }, ['is_safe' => ['html']]),

            new \Twig_SimpleFunction('t', function ($str) use ($localeCode) {

                return Yii::t($localeCode, $str);

            }, ['is_safe' => ['html']]),


            new \Twig_SimpleFunction('text_page_url_*', function ($code) {

                return Url::toRoute(['landings/text-page', 'id' => $this->landing->id, 'code' => $code, 'langCode' => Yii::$app->language]);

            }, ['is_safe' => ['html']]),


            new \Twig_SimpleFunction('get_*', function ($symbols) {

                return Yii::$app->request->get($symbols, '');

            }, ['is_safe' => ['html']]),
        ];


        $this->twig = new \Twig_Environment(new \Twig_Loader_Array(['']), [
            'cache' => Yii::getAlias($this->cachePath),
            'charset' => Yii::$app->charset,
            'autoescape' => false
        ]);


        $this->twig->setBaseTemplateClass('yii\twig\Template');

        $this->addFunctions($this->functions);
    }

    public function addVars($variables)
    {
        foreach ($variables as $name => $value) {
            $this->twig->addGlobal($name, $value);
        }
        return $this;
    }

    public function renderTemplate($html)
    {
        $template = $this->twig->createTemplate($html);

        return $template->render([]);
    }
}