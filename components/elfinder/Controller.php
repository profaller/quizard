<?php
/**
 * Date: 20.01.14
 * Time: 13:26
 */

namespace app\components\elfinder;

use app\models\Templates;
use mihaildev\elfinder\volume\Local;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


/**
 * Class Controller
 * @package mihaildev\elfinder
 * @property array $options
 */


class Controller extends BaseController{
	public $roots = [];
	public $disabledCommands = ['netmount'];
	public $watermark;

	private $_options;

	public function getOptions()
	{
		if($this->_options !== null)
			return $this->_options;

		$this->_options['roots'] = [];

		foreach($this->roots as $root){
			if(is_string($root))
				$root = ['path' => $root];

			if(!isset($root['class']))
				$root['class'] = Local::className();

			$root = Yii::createObject($root);

			/** @var \mihaildev\elfinder\volume\Local $root*/

			if($root->isAvailable())
				$this->_options['roots'][] = $root->getRoot();
		}

		if(!empty($this->watermark)){
			$this->_options['bind']['upload.presave'] = 'Plugin.Watermark.onUpLoadPreSave';

			if(is_string($this->watermark)){
				$watermark = [
					'source' => $this->watermark
				];
			}else{
				$watermark = $this->watermark;
			}

			$this->_options['plugin']['Watermark'] = $watermark;
		}

		$this->_options = ArrayHelper::merge($this->_options, $this->connectOptions);

		$this->_options['commandsOptions'] = [
        'edit' => [
            'mimes' => [],
            'editors' => [
                [
                    'mimes' => ['text/plain', 'text/html', 'text/javascript'],
                    'load' => new JsExpression('function(textarea) {
                                var mimeType = this.file.mime;
                                return CodeMirror.fromTextArea(textarea, {
                            mode: mimeType,
                            lineNumbers: true,
                            indentUnit: 4
                          });}'),

                    'save' => new JsExpression('function(textarea, editor) {
                            $(textarea).val(editor.getValue());
                        }')
                ]]
        ]
    ];

		return $this->_options;
	}

    public function actionConnect()
    {
        $class = $_REQUEST['modelClass'];

        if (!class_exists($class)) {
            return '';
        }

        $model = $class::findOne(['id' => $_REQUEST['model']]);

        if (!$model) {
           return '';
        }

        $this->roots = [
            [
                'baseUrl'=>'@web',
                'basePath'=>'@webroot',
                'path' => $model->uploadRelativePath(),
                'name' => $model->sid
            ],
        ];

        return $this->renderFile(__DIR__."/views/connect.php", ['options'=>$this->getOptions(), 'plugin' => $this->plugin]);
    }

    public function actionManager(){

        return $this->renderFile(__DIR__."/views/manager.php", ['options'=>$this->getManagerOptions()]);
    }
}
