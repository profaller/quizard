<?php

namespace app\components;

use app\models\Images;
use app\models\Landings;
use app\models\Steps;
use app\models\TextPage;
use app\services\FBPixelService;
use app\services\TrackerService;
use yii\base\Object;
use yii\helpers\Url;
use yii\web\HttpException;


class Land extends Object
{
    /**
     * @var Landings
     */
    public $landingModel;

    /**
     * @var Steps
     */
    public $stepModel;

    /**
     * @var Images
     */
    public $imageModel;

    /**
     * @var TextPage
     */
    public $textPageModel;

    public $langCode;


    public function init()
    {
        \Yii::$app->language = $this->langCode;
    }

    public function renderTextPage()
    {
        $html = preg_replace('#(\{\{\s*content\s*\}\})#is', $this->textPageModel->content, $this->compileBaseTemplate());

        echo (new LandingRenderer(['landing' => $this->landingModel]))->renderTemplate($html);
    }

    public function renderStep()
    {
        $html = preg_replace('#(\{\{\s*content\s*\}\})#is', $this->compileStepTemplate() . $this->getDepthTrackerImage(), $this->compileBaseTemplate());

        $vars = [
            'next_step_url' => $this->getNextUrl(),
            'offer_url' => TrackerService::create()->getOfferUrl(),
            'prize_image_url' => $this->imageModel->getUploadUrl('file'),
        ];

        echo (new LandingRenderer(['landing' => $this->landingModel]))->addVars($vars)->renderTemplate($html);
    }

    /**
     * @return mixed|string
     * @throws HttpException
     */
    protected function compileStepTemplate()
    {
        $stepFile = $this->stepModel->uploadPath() . '/template.html';
        if (!file_exists($stepFile)) {
            throw new HttpException(500);
        }
        $stepHtml = file_get_contents($stepFile);
        $stepBaseUrl = \Yii::$app->request->hostInfo . '/assets/la/steps/' . 'step_' . md5($this->stepModel->id) . '/';

        $stepHtml = preg_replace_callback(
            '#(src|href)=(\'|")([^\'">]+?)(\'|")#is',
            function ($matches) use ($stepBaseUrl) {
                $content = strpos($matches[3], '{{') !== false ? $matches[3] : $stepBaseUrl . $matches[3];

                return $matches[1] . '="' . $content . '"';
            },
            $stepHtml);


        $stepHtml = preg_replace_callback(
            '#(url)\s*\((\'|")*([^\'"\)>]+?)(\'|")*\)#is',
            function ($matches) use ($stepBaseUrl) {
                $content = strpos($matches[3], '{{') !== false ? $matches[3] : $stepBaseUrl . $matches[3];

                return 'url(' . $content . ')';
            },
            $stepHtml);

        return $stepHtml;
    }

    /**
     * @return mixed|string
     * @throws HttpException
     */
    protected function compileBaseTemplate()
    {
        $tplFile = $this->landingModel->template->uploadPath() . '/template.html';
        if (!file_exists($tplFile)) {
            throw new HttpException(500);
        }
        $html = file_get_contents($tplFile);

        $html = preg_replace(
            '#<head>(.+?)</head>#is',
            "<head>
                <base href='" . \Yii::$app->request->hostInfo . "/assets/la/tpls/" . 'tpl_' . md5($this->landingModel->template->sid) . "/' />
                " . FBPixelService::create()->getFBPixelScripts() . " 
                $1
             </head>", $html);

        return $html;
    }

    /**
     * @return string
     */
    public function getDepthTrackerImage()
    {
        return '<img src="' . TrackerService::create()->getDepthTrackerUrl($this->getCurrentStepOrder()) . '" height="1" width="1" class="depth-tracker" style="position: absolute; left: 1px; top: 1px; "/>';
    }

    /**
     * @return integer
     */
    public function getCurrentStepOrder()
    {
        foreach ($this->landingModel->steps as $i => $step) {
            if ($step->id == $this->stepModel->id) {
                return ++$i;
            }
        }

        return 1;
    }

    /**
     * @return string
     */
    public function getNextUrl()
    {
        $nextIndex = -1;
        foreach ($this->landingModel->steps as $i => $step) {
            if ($step->id == $this->stepModel->id) {
                $nextIndex = ++$i;
                break;
            }
        }

        if (!isset($this->landingModel->steps[$nextIndex])) {
            return Url::toRoute(['landings/view', 'id' => $this->landingModel->id, 'langCode' => $this->langCode, 'imgCode' => $this->imageModel->code]);
        }

        return Url::toRoute(['landings/view', 'id' => $this->landingModel->id, 'langCode' => $this->langCode, 'imgCode' => $this->imageModel->code, 'stepId' => $this->landingModel->steps[$nextIndex]->id]);
    }
}






















