<?php

namespace app\components;

use app\services\TrackerService;
use yii\base\Object;
use yii\web\UrlRuleInterface;


class LandUrlRule extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'landings/view' && isset($params['id'], $params['langCode'], $params['imgCode'])) {

            $url = '/v/' . $params['id'] . '/' . $params['langCode'] . '/' . $params['imgCode'] . '/';
            if (isset($params['stepId'])) {
                $url .= $params['stepId'] . '/';
            }

            return $url;

        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if (preg_match('#^v/(\d+?)/(\w+?)/([\w\d_-]+)/?(\d+?)?/$#', $pathInfo, $matches)) {

            list(, $id, $langCode, $imgCode) = $matches;

            $stepId = false;
            if (isset($matches[4])) {
                $stepId = $matches[4];
            }

            $params = compact('id', 'langCode', 'imgCode', 'stepId');

            \Yii::$app->session->set('__land_view_params', $params);

            TrackerService::create()->saveTrackParams();

            \Yii::$app->response->redirect('/v1/');
            \Yii::$app->response->send();

            return ['landings/view', $params];

        } elseif (preg_match('#^v1/$#', $pathInfo, $matches)) {

            $params = \Yii::$app->session->get('__land_view_params');
            //dbg($params);

            if ($params) {
                return ['landings/view', $params];
            }

        }

        return false;
    }
}