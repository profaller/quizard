<?php
namespace app\components;

use yii\db\ActiveRecord;
use yii\base\Behavior;

class ArrayAttributesBehavior extends Behavior
{
    public $attributes = [];


    public function events()
    {
        return [
            ActiveRecord::EVENT_INIT            => 'unpackAttributes',
            ActiveRecord::EVENT_AFTER_INSERT    => 'unpackAttributes',
            ActiveRecord::EVENT_AFTER_UPDATE    => 'unpackAttributes',
            ActiveRecord::EVENT_AFTER_FIND      => 'unpackAttributes',

            ActiveRecord::EVENT_BEFORE_INSERT   => 'packAttributes',
            ActiveRecord::EVENT_BEFORE_UPDATE   => 'packAttributes',
        ];
    }

    public function unpackAttributes()
    {
        foreach ($this->attributes as $attribute) {
            $data = unserialize($this->owner->{$attribute});
            if (!$data) {
                $data = [];
            }
            $this->owner->{$attribute} = $data;
        }
    }

    public function packAttributes()
    {
        foreach ($this->attributes as $attribute) {
            if (!is_array($this->owner->{$attribute})) {
                $this->owner->{$attribute} = [];
            }
            $value = [];
            $i = 0;
            foreach ($this->owner->{$attribute} as $k => $val) {
                $key = is_string($k) ? $k : $i++;
                if (is_array($val) && count($val)) {
                    $value[$key] = $val;
                } else if (strlen(trim($val)) > 0) {
                    $value[$key] = $val;
                }
            }
            $this->owner->{$attribute} = serialize($value);
        }
    }
}