(function($) {

    App.Widgets.landings = {};


    App.Widgets.landings.textPageForm = can.Control.extend(
        {
            pluginName: 'textPageForm'
        },
        {
            init: function () {

                /*CodeMirror.fromTextArea($('.js-code-mirror-html')[0], {
                    lineNumbers: true,
                    mode : "text/html",
                    htmlMode: true,
                    extraKeys: {
                        "Shift-Space": "autocomplete"
                    }
                });*/
            }
        }
    );



    App.Widgets.landings.Form = can.Control.extend(
        {
                pluginName: 'land_form'
        },
        {
            init: function () {
                var self = this;

                this.newCount = 501;
                this.proto = this.element.find('.js-proto');
                this.sort = this.element.find('.js-sort');


                this.sort.sortable({
                    items: '.js-stepModel-item',
                    handle: '.js-handle',
                    stop: function( event, ui ) {

                        self.element.find('.js-stepModel-order').each(function (i, el) {
                            $(el).val(++i);
                        });

                    }
                });
            },

            '.js-step-select change': function (el, e) {
                el.next().attr('name', el.val().length ? 'steps_order_' + el.val() : '')
            },

            '.js-add-proto click': function (btn, e) {
                var newBlock = this.proto.clone();

                newBlock.find('.js-control').attr('name', 'Landings[steps_ids][]');
                newBlock.find('.js-hidden-control').val(this.newCount);


                this.newCount++;

                newBlock.show().removeClass('js-proto').insertBefore(btn);
            },

            '.js-delete click': function (el, e) {
                e.preventDefault();

                el.closest('.js-stepModel-item').remove();
            }

        }
    );


  App.Widgets.landings.List = can.Control.extend(
        {
                pluginName: 'land_open_link'
        },
        {
            init: function () {
                this.link = this.element.find('.js-link');
                this.id = this.element.data('id');

                this.bildLink();
            },

            'select change': function (el, e) {
                this.bildLink();
            },

            bildLink: function () {
                var vars = this.element.find('select'),
                    url = '/v/' + this.id + '/' + vars.eq(0).val() + '/' + vars.eq(1).val() + '/' + vars.eq(2).val() + '/';
                this.link.text(url).attr('href', url);
            }

        }
    );

    App.Widgets.landings.TxtOpenLink = can.Control.extend(
        {
            pluginName: 'text_open_link'
        },
        {
            init: function () {
                this.link = this.element.find('.js-link');
                this.code = this.element.data('code');

                this.bildLink();
            },

            'select change': function (el, e) {
                this.bildLink();
            },

            bildLink: function () {
                var vars = this.element.find('select'),
                    url = '/info/' + vars.eq(0).val() + '/' + vars.eq(1).val() + '/' + this.code + '/';

                this.link.text(url).attr('href', url);
            }

        }
    );

})(jQuery);