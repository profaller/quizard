(function($) {

    App.Widgets.locales = {};


    App.Widgets.locales.Sorts = can.Control.extend(
        {
            pluginName: 'loc_sorts'
        },
        {
            init: function () {
                var self = this;

                this.newCount = 501;
                this.proto = this.element.find('.js-proto');
                this.sort = this.element;


                this.sort.sortable({
                    items: '.js-sort-item',
                    handle: '.js-handle',
                    stop: function( event, ui ) {

                        self.element.find('.js-sort-order').each(function (i, el) {
                            $(el).val(++i);
                        });

                    }
                });
            },

            '.js-step-select change': function (el, e) {
                el.next().attr('name', el.val().length ? 'steps_order_' + el.val() : '')
            },

            '.js-add-proto click': function (btn, e) {
                var newBlock = this.proto.clone();

                newBlock.find('.js-control').attr('name', 'Landings[steps_ids][]');
                newBlock.find('.js-hidden-control').val(this.newCount);


                this.newCount++;

                newBlock.show().removeClass('js-proto').insertBefore(btn);
            }

        }
    );


    App.Widgets.locales.AddRow = can.Control.extend(
        {
                pluginName: 'loc_AddRow'
        },
        {
            init: function () {
                var self = this;

                this.newCount = 0;

                this.proto = this.element.find('.js-proto');

                this.element.find('.js-sort').sortable({
                    items: '.js-sort-item',
                    handle: '.js-handle',
                    stop: function( event, ui ) {
                        self.resort();
                    }
                });
            },

            resort: function () {
                this.element.find('.js-sort-order').each(function (i, el) {
                    $(el).val(++i);
                });
            },

            '.js-add-proto click': function (btn, e) {
                var newBlock = this.proto.clone();

                newBlock.find('.js-src-control').each(this.proxy(function (i, el) {
                    var control = $(el),
                        name = control.attr('name');

                    control.attr('name', 'newSourceMessage['+ this.newCount +']['+ name +']')
                }));


                newBlock.find('.js-cell-control').each(this.proxy(function (i, el) {
                    var control = $(el),
                        name = control.attr('name');

                    control.attr('name', 'newMessage['+ this.newCount +'_'+ name +']')
                }));

                newBlock.show().removeClass('js-proto').addClass('js-sort-item').insertBefore(this.proto);

                this.newCount++;
                this.resort();
            },

            '.js-delete click': function (el, e) {
                e.preventDefault();

                if (!confirm('Delete item?')) {
                    return;
                }

                $.ajax({
                    url: el.attr('href'),
                    type: 'POST'
                });

                el.closest('tr').remove();
            }

        }
    );



})(jQuery);