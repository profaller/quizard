<?php

use app\services\TrackerService;

$fbpixel = TrackerService::create()->getFBPixel();

if ($fbpixel) { ?>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', '//connect.facebook.net/en_US/fbevents.js');

    </script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $fbpixel; ?>&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->

<?php } ?>

<script>
    if (typeof fbq == 'undefined') {
        fbq = function () {
        }
    }
    if (typeof beacon == 'undefined') {
        window.beacon = function (a) {
            a = a || {};
            a.url = a.url || null;
            a.vars = a.vars || {};
            a.error = a.error || function () {
                };
            a.success = a.success || function () {
                };
            var b = [];
            for (var c in a.vars) {
                b.push(c + '=' + a.vars[c])
            }
            var d = b.join('&');
            if (a.url) {
                var e = new Image();
                if (e.onerror) {
                    e.onerror = a.error
                }
                if (e.onload) {
                    e.onload = a.success
                }
                e.src = a.url + '?' + d
            }
        };
    }
</script>