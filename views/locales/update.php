<?php

use app\models\Languages;
use app\models\Message;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Locales */

$this->title = 'Update Locale: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Locales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="locales-update">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#grid" data-toggle="tab">Grid</a></li>
        <li><a href="#import" data-toggle="tab">Import</a></li>
    </ul>

    <br>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="grid">
            <?= $this->render('_grid', compact('model'))?>
        </div>

        <div class="tab-pane" id="import">
            <?= $this->render('_import', compact('model'))?>
        </div>
    </div>


</div>
