<?php

use app\models\Languages;
use app\models\Message;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Locales */


?>

    <div class="locales-form" data-app-controller="loc_AddRow">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <br>
        <br>

        <table class="table table-striped table-hover locales-table js-sort">
            <tr>
                <th></th>
                <th>Placeholder</th>

                <?php foreach (Languages::find()->all() as $lang) {?>
                    <th><?= $lang->name?> (<?= $lang->code?>)</th>
                <?php } ?>

                <th></th>
            </tr>


            <?php foreach ($model->sourceMessages as $i => $srcModel) { ?>
                <tr class="js-sort-item">
                    <td class="js-handle locales-table__sort-handle">
                        <i class="fa fa-arrows" aria-hidden="true"></i>
                    </td>
                    <td>
                        <?= Html::activeTextInput($srcModel, '['.$i.']message', ['class' => ''])?>
                        <?= Html::activeHiddenInput($srcModel, '['.$i.']order', ['class' => 'js-sort-order'])?>
                    </td>

                    <?php foreach (Languages::find()->all() as $lang) {
                        $message = Message::find()->andWhere(['language' => $lang->code, 'id' => $srcModel->id])->one()?>

                        <td>
                            <?php echo Html::textInput("Message[{$srcModel->id}_{$lang->code}]", $message ? $message->translation : null); ?>
                        </td>

                    <?php } ?>

                    <td>
                        <?= Html::a('X', ['delete-src', 'id' => $srcModel->id], ['class' => 'btn btn-danger btn-sm js-delete'])?>
                    </td>
                </tr>
            <?php } ?>


            <tr class="js-proto" style="display: none">
                <td class="js-handle locales-table__sort-handle">
                    <i class="fa fa-arrows" aria-hidden="true"></i>
                </td>
                <td>
                    <?= Html::textInput('message', null, ['class' => 'js-src-control'])?>
                    <?= Html::hiddenInput('order', null, ['class' => 'js-src-control js-sort-order'])?>
                </td>

                <?php foreach (Languages::find()->all() as $lang) { ?>

                    <td>
                        <?php echo Html::textInput("{$lang->code}", null, ['class' => 'js-cell-control']); ?>
                    </td>

                <?php } ?>

                <td>
                    <?= Html::a('X', ['delete-src', 'id' => -1], ['class' => 'btn btn-danger btn-sm js-delete'])?>
                </td>
            </tr>

        </table>


        <div class="form-group clearfix">
            <a class="btn btn-default pull-left js-add-proto">+</a>
        </div>


        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>

