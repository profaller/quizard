<?php

use app\models\Languages;
use app\models\Message;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Locales */


?>

<div class="locales-form" >

    <?php $form = ActiveForm::begin([
        'action' => Url::toRoute(['locales/import', 'id' => $model->id]),
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= Html::fileInput('import_file') ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton('Import', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>

