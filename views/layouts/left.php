<aside class="main-sidebar">

    <section class="sidebar">

        <?php
        $auth = Yii::$app->authManager;
        echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [

                    ['label' => 'Landings', 'icon' => 'circle', 'url' => ['/landings/index'], 'visible' => Yii::$app->user->can('admin')],
                    ['label' => 'Templates', 'icon' => 'circle', 'url' => ['/templates/index'], 'visible' => Yii::$app->user->can('admin')],
                    ['label' => 'Pages (Steps)', 'icon' => 'circle', 'url' => ['/steps/index'], 'visible' => Yii::$app->user->can('admin')],
                    ['label' => 'Languages', 'icon' => 'circle', 'url' => ['/languages/index'], 'visible' => Yii::$app->user->can('admin')],
                    ['label' => 'Localization', 'icon' => 'circle', 'url' => ['/locales/index'], 'visible' => Yii::$app->user->can('admin')],
                    ['label' => 'Images', 'icon' => 'circle', 'url' => ['/images/index'], 'visible' => Yii::$app->user->can('admin')],
                    ['label' => 'Text Pages', 'icon' => 'circle', 'url' => ['/text-pages/index'], 'visible' => Yii::$app->user->can('admin')],
                    ['label' => 'Cloudflare', 'icon' => 'circle', 'url' => ['/cloudflare/index'], 'visible' => Yii::$app->user->can('admin')],

                    /*[
                        'label' => Yii::t('app', 'Квизы'),
                        'icon' => 'fa fa-bars',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Обрамляющие шаблоны', 'icon' => 'fa fa-circle', 'url' => ['/categories/index'], 'visible' => Yii::$app->user->can('admin')],
                            ['label' => 'Страницы (Шаги)', 'icon' => 'fa fa-circle', 'url' => ['/users/index'], 'visible' => Yii::$app->user->can('admin')],
                        ],
                    ],*/
                ],
            ]
        ) ?>

    </section>

</aside>
