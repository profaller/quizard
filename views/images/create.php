<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Images */

$this->title = 'Create Image';
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="images-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
