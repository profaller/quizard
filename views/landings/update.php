<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Landings */

$this->title = 'Update Landing: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Landings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="landings-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
