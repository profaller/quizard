<?php

use app\models\Landings;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$view = $this;

$this->title = 'Landings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landings-index">

    <p>
        <?= Html::a('Create Landing', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="open-link">
        <span>Open Link:</span> <pre>/v/{landing ID}/{language code}/{image code}/[{step id}/]</pre>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        'columns' => [

            'id',
            'name',
            'template.name:raw:Template',
            'locale.name:raw:Locale',
            [
                'format' => 'raw',
                'label' => 'Open',
                'value' => function ($model) use ($view) {
                    return $view->render('_open_link', compact('model'));
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}',
                /*'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon glyphicon-eye-open"></span>',
                            ['landings/view' ,'id' => $model->id, ''],

                            [
                                'data-pjax' => '0',
                                'target' => '_blank',
                            ]
                        );
                    },
                ],*/
            ],
        ],
    ]); ?>
</div>
