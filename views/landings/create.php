<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Landings */

$this->title = 'Create Landing';
$this->params['breadcrumbs'][] = ['label' => 'Landings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landings-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
