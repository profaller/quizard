<?php
/**
 * @var Land $land
 *
 */

use app\components\Land;
use app\services\LandingAssetsService;


LandingAssetsService::registerAssets($land->landingModel, $this);

$land->renderStep();


