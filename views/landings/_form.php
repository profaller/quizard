<?php

use app\models\Locales;
use app\models\Steps;
use app\models\Templates;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Landings */
/* @var $form yii\widgets\ActiveForm */


$stepsList = ArrayHelper::map(Steps::find()->all(), 'id', 'name');

//dbg($model->steps);
?>

<div class="landings-form" data-app-controller="land_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'template_id')->dropDownList(ArrayHelper::map(Templates::find()->all(), 'id', 'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'locale_id')->dropDownList(ArrayHelper::map(Locales::find()->all(), 'id', 'name'), ['prompt' => '']) ?>

    <?= Html::hiddenInput('Landings[steps_ids]', 0)?>


    <div class="js-sort">

        <?php foreach ($model->steps as $step) {?>

            <div class="js-step-item step-item">
                <div class="item-wrapper clearfix">
                    <div class="sort-handle pull-left js-handle">
                        <i class="fa fa-arrows" aria-hidden="true"></i>
                    </div>
                    <div class="select-area pull-left">
                        <?= Html::dropDownList('Landings[steps_ids][]', $step->id, $stepsList, ['class' => 'form-control js-stepModel-select'])?>
                        <?= Html::hiddenInput('steps_order_' . $step->id, $step->m2mLadingsModel($model->id)->order, ['class' => 'js-stepModel-order'])?>
                    </div>
                    <div class="pull-left">
                        <?= Html::a('X', '#', ['class' => 'btn btn-danger js-delete'])?>
                    </div>
                </div>
            </div>

        <?php } ?>

            <a class="btn btn-default js-add-proto">+</a>

    </div>


    <div class="js-step-item js-proto step-item" style="display: none">
        <div class="item-wrapper clearfix">
            <div class="sort-handle pull-left js-handle">
                <i class="fa fa-arrows" aria-hidden="true"></i>
            </div>
            <div class="select-area pull-left">
                <?= Html::dropDownList('', null, $stepsList, ['class' => 'form-control js-control js-stepModel-select', 'prompt' => ''])?>
                <?= Html::hiddenInput('steps_order_', 0, ['class' => 'js-hidden-control js-stepModel-order'])?>
            </div>
            <div class="pull-left">
                <?= Html::a('X', '#', ['class' => 'btn btn-danger js-delete'])?>
            </div>
        </div>
    </div>


    <br>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
