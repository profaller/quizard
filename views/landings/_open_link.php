<?php

use app\models\Images;
use app\models\Landings;
use app\models\Languages;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Landings */

?>

<div class="landings-index__open-link" data-app-controller="land_open_link" data-id="<?= $model->id?>">
    <label for="">Lang:</label> <?= Html::dropDownList('', null, ArrayHelper::map(Languages::find()->all(), 'code', 'name'))?>
    /
    <label for="">Image:</label> <?= Html::dropDownList('', null, ArrayHelper::map(Images::find()->all(), 'code', 'name'))?>
    /
    <label for="">Step:</label> <?= Html::dropDownList('', null, ArrayHelper::map($model->steps, 'id', 'name'))?>

    &nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;

    <a href="#" class="js-link" target="_blank"></a>
</div>
