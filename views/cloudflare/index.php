<?php

use app\models\Languages;
use app\models\Message;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Locales */

$this->title = 'Cloudflare';
?>
<div class="cloudf">

    <div class="cloudf-form" >

        <?php if (Yii::$app->session->hasFlash('cf')) {?>

            <div class="alert alert-success">
                <?= Yii::$app->session->getFlash('cf')?>
            </div>

        <?php } ?>

        <?php $form = ActiveForm::begin([
            'action' => Url::toRoute(['cloudflare/reset-cache']),
        ]); ?>


        <div class="form-group">
            <?= Html::submitButton('Reset Cache', ['class' => 'btn btn-primary']) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>

</div>
