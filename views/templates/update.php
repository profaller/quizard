<?php

use app\components\elfinder\ElFinder;
use app\models\Templates;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Templates */

$this->title = 'Update Template: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="templates-update">

    <div class="templates-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'sid')->textInput(['disabled' => 'disabled']) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Save To Continue' : 'Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        <div><h3>Template Files <span style="font-size: .6em">(all paths must be relative)</span></h3> </div>

        <?= ElFinder::widget([
            'modelClass' => Templates::className(),
            'model' => $model->id,
            'language' => 'ru',
            'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            'callbackFunction' => new JsExpression('function(file, id){}'), // id - id виджета
            'frameOptions' => [
                'style' => 'height: 500px; width: 100%; border: none;'
            ]
        ]);?>

        <div class="panel panel-default template-editor__legends">

            <div class="panel-heading">
                Available functions and variables
            </div>

            <div class="panel-body">

                <div class="legend-item">
                    <pre>{{ content }}</pre>
                    <div class="template-editor__legends__description">
                        Main content area.
                    </div>
                </div>

                <div class="legend-item">
                    <pre>{{ text_page_url_* }}</pre>
                    <div class="template-editor__legends__description">
                        Url to text page. * - text page code.
                    </div>
                </div>

                <div class="legend-item">
                    <pre>{{ t("TRANSLATE_PHRASE") }}</pre>
                    <div class="template-editor__legends__description">
                        Translate function.
                    </div>
                </div>

                <div class="legend-item">
                    <pre>{{ offer_url }}</pre>
                    <div class="template-editor__legends__description">
                        Url to offer.
                    </div>
                </div>

                <div class="legend-item">
                    <pre>{{ next_step_url }}</pre>
                    <div class="template-editor__legends__description">
                        Url to next step page.
                    </div>
                </div>

                <div class="legend-item">
                    <pre>{{ prize_image_url }}</pre>
                    <div class="template-editor__legends__description">
                        Url to image.
                    </div>
                </div>



            </div>
        </div>

    </div>

</div>
