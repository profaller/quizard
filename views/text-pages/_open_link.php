<?php

use app\models\Landings;
use app\models\Languages;
use app\models\TextPage;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model TextPage */

?>

<div class="landings-index__open-link" data-app-controller="text_open_link" data-code="<?= $model->code?>">

    <label for="">Landing:</label> <?= Html::dropDownList('', null, ArrayHelper::map(Landings::find()->all(), 'id', 'name'))?>
    /
    <label for="">Lang:</label> <?= Html::dropDownList('', null, ArrayHelper::map(Languages::find()->all(), 'code', 'name'))?>

    &nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;

    <a href="#" class="js-link" target="_blank"></a>
</div>
