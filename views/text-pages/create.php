<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TextPage */

$this->title = 'Create Text Page';
$this->params['breadcrumbs'][] = ['label' => 'Text Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-page-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
