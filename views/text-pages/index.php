<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$view = $this;

$this->title = 'Text Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-page-index">

    <p>
        <?= Html::a('Create Text Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'name',
            'code',
            [
                'format' => 'raw',
                'label' => 'Open',
                'value' => function ($model) use ($view) {
                    return $view->render('_open_link', compact('model'));
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>
</div>
