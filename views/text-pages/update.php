<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TextPage */

$this->title = 'Update Text Page: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Text Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="text-page-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
