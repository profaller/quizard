<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'js/vendor/ui/jquery-ui.min.css',
        'js/vendor/ui/jquery-ui.structure.min.css',
        'js/vendor/ui/jquery-ui.theme.min.css',

        'css/site.css',
    ];
    public $js = [
        'js/vendor/ui/jquery-ui.min.js',

        'js/vendor/can.custom.js',
        'js/app/app.main.js',
        'js/app/app.common.js',
        'js/app/app.locales.js',
        'js/app/app.landings.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
