<?php

namespace app\assets;

use yii\web\AssetBundle;

class CodeMirrorAsset extends AssetBundle
{
    public $sourcePath = '@vendor/profaller/CodeMirror';
    public $css = [
        'lib/codemirror.css',
        'addon/hint/show-hint.css',
        'addon/dialog/dialog.css',
    ];
    public $js = [
        ['lib/codemirror.js', 'position' => \yii\web\View::POS_END],

        ['addon/hint/show-hint.js', 'position' => \yii\web\View::POS_END],
        ['addon/hint/xml-hint.js', 'position' => \yii\web\View::POS_END],
        ['addon/hint/html-hint.js', 'position' => \yii\web\View::POS_END],

        ['addon/search/search.js', 'position' => \yii\web\View::POS_END],
        ['addon/search/searchcursor.js', 'position' => \yii\web\View::POS_END],
        ['addon/search/jump-to-line.js', 'position' => \yii\web\View::POS_END],

        ['addon/dialog/dialog.js', 'position' => \yii\web\View::POS_END],

        ['mode/xml/xml.js', 'position' => \yii\web\View::POS_END],
        ['mode/javascript/javascript.js', 'position' => \yii\web\View::POS_END],
        ['mode/css/css.js', 'position' => \yii\web\View::POS_END],
        ['mode/htmlmixed/htmlmixed.js', 'position' => \yii\web\View::POS_END],
    ];
}
