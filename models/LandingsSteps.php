<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "landings_steps".
 *
 * @property int $landing_id
 * @property int $step_id
 * @property int $order
 *
 * @property Landings $landing
 * @property Steps $stepModel
 */
class LandingsSteps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landings_steps';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['landing_id', 'step_id'], 'required'],
            [['landing_id', 'step_id', 'order'], 'integer'],
            [['landing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Landings::className(), 'targetAttribute' => ['landing_id' => 'id']],
            [['step_id'], 'exist', 'skipOnError' => true, 'targetClass' => Steps::className(), 'targetAttribute' => ['step_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'landing_id' => 'Landing ID',
            'step_id' => 'Step ID',
            'order' => 'Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanding()
    {
        return $this->hasOne(Landings::className(), ['id' => 'landing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStep()
    {
        return $this->hasOne(Steps::className(), ['id' => 'step_id']);
    }
}
