<?php

namespace app\models;

use app\components\UploadFilesBehavior;
use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property string $file
 * @property int $is_active
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadFilesBehavior::className(),
                'attributes' => ['file'],
                'uploadPath' => Yii::getAlias('@webroot/uploads/images/'),
                'uploadUrl' => '/uploads/images/',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['name', 'code'], 'string', 'max' => 255],
            [['code'], 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9]/', 'message' => 'Invalid characters in code',],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'code' => 'Code',
            'file' => 'File',
            'is_active' => 'Is Active',
        ];
    }
}
