<?php

namespace app\models\enum;


use app\components\Enum;

class Role extends Enum
{
    const ADMIN        = 'admin';


    public static function getNamesList()
    {
        return [
            self::ADMIN    => \Yii::t('app', 'Admin'),
        ];
    }

    public static function getList()
    {
        return [
            self::ADMIN,
        ];
    }


}