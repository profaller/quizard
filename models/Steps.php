<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "steps".
 *
 * @property int $id
 * @property string $sid
 * @property string $name
 * @property integer $is_game
 *
 * @property LandingsSteps[] $landingsSteps
 */
class Steps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'steps';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['sid', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9_-]/', 'message' => 'Invalid characters in sid.',],

            [['name', 'sid'], 'required'],
            [['sid'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 255],
            [['sid'], 'unique'],
            [['is_game'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sid' => 'Template code (folder name)',
            'name' => 'Name',
            'is_game' => 'Game',
        ];
    }

    public function uploadPath()
    {
        return Yii::getAlias('@webroot/' . $this->uploadRelativePath());
    }

    public function uploadRelativePath()
    {
        return 'uploads/games/' . $this->sid;
    }

    public function beforeDelete()
    {
        FileHelper::removeDirectory($this->uploadPath());

        return parent::beforeDelete();
    }

    public function getLandingsSteps()
    {
        return $this->hasMany(LandingsSteps::className(), ['step_id' => 'id']);
    }

    /**
     * @param $landId
     * @return LandingsSteps
     */
    public function m2mLadingsModel($landId)
    {
        return $this->hasOne(LandingsSteps::className(), ['step_id' => 'id'])
            ->andWhere([LandingsSteps::tableName() . '.landing_id' => $landId])->one();
    }
}
