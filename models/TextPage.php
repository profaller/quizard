<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "text_page".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $content
 */
class TextPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'text_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['content'], 'string'],
            [['name', 'code'], 'string', 'max' => 255],
            [['code'], 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9_-]/', 'message' => 'Invalid characters in code',],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code (url)',
            'content' => 'Content',
        ];
    }
}
