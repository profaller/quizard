<?php

namespace app\models;

use app\components\ManyToManyBehavior;
use Yii;

/**
 * This is the model class for table "landings".
 *
 * @property int $id
 * @property string $name
 * @property int $template_id
 * @property int $locale_id
 *
 * @property Templates $template
 * @property LandingsSteps[] $landingsSteps
 * @property Steps[] $steps
 * @property Locales $locale
 */
class Landings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landings';
    }

    public function behaviors()
    {
        return [
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [

                    'steps_ids' => [
                        'steps',
                        'viaTableValues' => [
                            'order' => function($model, $relationName, $attributeName, $relatedPk) {
                                return Yii::$app->request->post('steps_order_' . $relatedPk, 500);
                            }
                        ]
                    ]

                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['steps_ids'], 'filter', 'filter' => function ($value) {
                $result = [];
                if (is_array($value)) {
                    foreach ($value as $v) {
                        if (!empty($v)) {
                            $result[] = $v;
                        }
                    }
                }
                return $result;
            }],
            [['name', 'template_id', 'locale_id'], 'required'],
            [['template_id', 'locale_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['steps_ids'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'template_id' => 'Template',
            'locale_id' => 'Locale',
            'steps' => 'Steps',
        ];
    }

    public function getTemplate()
    {
        return $this->hasOne(Templates::className(), ['id' => 'template_id']);
    }

    public function getLocale()
    {
        return $this->hasOne(Locales::className(), ['id' => 'locale_id']);
    }

    public function getLandingsSteps()
    {
        return $this->hasMany(LandingsSteps::className(), ['landing_id' => 'id']);
    }

    public function getSteps()
    {
        return $this->hasMany(Steps::className(), ['id' => 'step_id'])
            ->via('landingsSteps')
            ->joinWith('landingsSteps')
            ->andWhere([LandingsSteps::tableName() . '.landing_id' => $this->id])
            ->orderBy(LandingsSteps::tableName() . '.order asc');
    }
}
