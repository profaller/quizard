<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "templates".
 *
 * @property int $id
 * @property string $sid
 * @property string $name
 */
class Templates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['sid', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9_]/', 'message' => 'Invalid characters in sid.',],

            [['name', 'sid'], 'required'],
            [['sid'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 255],
            [['sid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sid' => 'Template Code (folder name)',
            'name' => 'Name',
        ];
    }

    public function uploadPath()
    {
        return Yii::getAlias('@webroot/' . $this->uploadRelativePath());
    }

    public function uploadRelativePath()
    {
        return 'uploads/templates/' . $this->sid;
    }

    public function beforeDelete()
    {
        FileHelper::removeDirectory($this->uploadPath());

        return parent::beforeDelete();
    }
}
