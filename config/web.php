<?php

function dbg($var)
{
    while (@ob_end_clean()) {}
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    die();
}

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'Quizard',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Europe/Moscow',


    'language' => 'en-US',
    'defaultRoute' => '/site/login',
    'homeUrl' => '/administrator/landings/',


    'controllerMap' => [
        'elfinder' => [
            'class' => 'app\components\elfinder\Controller',
            'access' => ['@'],
            'disabledCommands' => ['netmount'],
        ]
    ],


    'components' => [

        'request' => [
            'cookieValidationKey' => '-8hyrGS5DADhe6xhvDudg1ZUae3_6nfE',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'linkAssets' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [['jquery.min.js', 'position' => \yii\web\View::POS_HEAD]]
                ],
            ],
        ],

        'cloudflare' => [
            'class'         => 'app\components\CloudflareApi',
            'apiurl'   => 'https://api.cloudflare.com/client/v4/',
            'authkey'       => '5gds0kfdsc024ndsofsj049jisdofjsd034jw',
            'authemail'     => 'admin@mail.com',
            'sites'         => [
                'quizard.dev',
            ],
        ],

        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'sourceLanguage' => 'en-US',
                    //'basePath' => '@app/messages',
                    /*'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],*/
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'suffix' => '/',
            'rules' => [
                '/' => 'site/offline',

                'administrator' => 'site/login',

                'info/<id:\d+>/<langCode:\w+>/<code:[\w_-]+>' => 'landings/text-page',
                ['class' => 'app\components\LandUrlRule'],

                'administrator/<controller>'                      => '<controller>/index',
                'administrator/<controller>/<action>'             => '<controller>/<action>',

                '<controller>/<action>' => 'site/offline',
                '<controller>' => 'site/offline',
            ],
        ],
        'formatter' => [
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y h:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ',',
            'currencyCode' => 'EUR',
            'sizeFormatBase' => 1000
        ],
        'db' => require(__DIR__ . '/db.php'),

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
