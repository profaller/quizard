<?php

namespace app\services;



use app\models\Landings;
use app\models\Steps;
use yii\helpers\ArrayHelper;

class LandingAssetsService
{
    protected static $templateAssetClass = 'class LandingTemplateAsset_<tpl_code> extends \yii\web\AssetBundle { public $sourcePath = "@webroot/uploads/templates/<tpl_code>";}';
    protected static $stepAssetClass = 'class LandingStepAsset_<step_code> extends \yii\web\AssetBundle { public $sourcePath = "@webroot/uploads/games/<step_code>";}';


    /**
     * @param Landings $model
     * @param $view
     */
    public static function registerAssets($model, $view)
    {
        \Yii::$app->assetManager->linkAssets = true;
        \Yii::$app->assetManager->basePath = \Yii::getAlias('@webroot/assets/la/tpls');
        \Yii::$app->assetManager->hashCallback = function () use ($model) {
            return 'tpl_' . md5($model->template->sid);
        };

        eval(str_replace('<tpl_code>', $model->template->sid, self::$templateAssetClass));
        $class = 'LandingTemplateAsset_' . $model->template->sid;
        $class::register($view);

        foreach ($model->steps as $step) {
            \Yii::$app->assetManager->basePath = \Yii::getAlias('@webroot/assets/la/steps');
            \Yii::$app->assetManager->hashCallback = function () use ($step) {
                return 'step_' . md5($step->id);
            };

            eval(str_replace('<step_code>', $step->sid, self::$stepAssetClass));
            $class = 'LandingStepAsset_' . $step->sid;
            $class::register($view);
        }
    }
}