<?php

namespace app\services;




class FBPixelService
{
    private static  $_instance = null;


    private final function __construct(){}

    /**
     * @return $this
     */
    public static function create()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getFBPixelScripts()
    {
        return \Yii::$app->controller->view->render('//_services/fb_pixel');
    }

    public function getFBPixelAction()
    {
        $fbpixel = TrackerService::create()->getFBPixel();
        $fbevent = TrackerService::create()->getFBEvent();

        $str = '';
        if ($fbpixel) {
            $str = "
                <script>
                    fbq('init', '$fbpixel');
                    fbq('track', '$fbevent');
                </script>";
        }

        return $str;
    }
}