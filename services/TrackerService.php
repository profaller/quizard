<?php

namespace app\services;



class TrackerService
{
    private static  $_instance = null;


    private final function __construct(){}

    /**
     * @return $this
     */
    public static function create()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    public function saveTrackParams()
    {
        $getOrigin = \Yii::$app->request->get('origin', false);
        $getSubid = \Yii::$app->request->get('subid', false);
        $fbpixel = \Yii::$app->request->get('fbpixel', false);
        $fbevent = \Yii::$app->request->get('fbevent', false);

        if ($getOrigin) {
            \Yii::$app->session->set('__origin', $getOrigin);
        }
        if ($getSubid) {
            \Yii::$app->session->set('__subid', $getSubid);
        }
        if ($fbpixel) {
            \Yii::$app->session->set('__fbpixel', $fbpixel);
        }
        if ($fbevent) {
            \Yii::$app->session->set('__fbevent', $fbevent);
        }
    }

    public function getSubId()
    {
        return \Yii::$app->session->get('__subid', false);
    }

    public function getOrigin()
    {
        return \Yii::$app->session->get('__origin', false);
    }

    public function getFBPixel()
    {
        return \Yii::$app->session->get('__fbpixel', false);
    }

    public function getFBEvent()
    {
        return \Yii::$app->session->get('__fbevent', 'PageView');
    }

    public function getDepthTrackerUrl($depth)
    {
        $subId = $this->getSubId();

        if ($subId) {
            return 'http://click.zeustrak.com/depth/?' . http_build_query([
                    'depth' => $depth,
                    's' => $subId,
                    'cid' => $subId,
                    'nc' => time(),
                ]);
        }

        return false;
    }

    public function getOfferUrl()
    {
        return 'http://click.zeustrak.com/click/1?origin=' . $this->getOrigin();
    }
}