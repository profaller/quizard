<?php

use yii\db\Migration;

class m170417_185514_images extends Migration
{
    public function safeUp()
    {
        $this->createTable('images', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'code' => $this->string()->notNull(),
            'file' => $this->string(),
            'is_active' => $this->boolean()->notNull()->defaultValue(true),
        ]);
    }

    public function safeDown()
    {
        echo "m170417_185514_images cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170417_185514_images cannot be reverted.\n";

        return false;
    }
    */
}
