<?php

use yii\db\Migration;

class m170417_195019_text_pages extends Migration
{
    public function safeUp()
    {
        $this->createTable('text_page', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string()->notNull()->unique(),
            'content' => $this->text()
        ]);
    }

    public function safeDown()
    {
        echo "m170417_195019_text_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170417_195019_text_pages cannot be reverted.\n";

        return false;
    }
    */
}
