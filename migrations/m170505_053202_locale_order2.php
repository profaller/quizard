<?php

use app\models\Locales;
use app\models\SourceMessage;
use yii\db\Migration;

class m170505_053202_locale_order2 extends Migration
{
    public function safeUp()
    {

        /** @var Locales $lModel */
        foreach (Locales::find()->all() as $lModel) {

            foreach ($lModel->sourceMessages as $i => $sourceMessage) {
                $sourceMessage->order = $i + 1;
                $sourceMessage->category = $lModel->id;
                $sourceMessage->save(false);
            }

        }

        $this->dropColumn(Locales::tableName(), 'code');
    }

    public function safeDown()
    {
        echo "m170505_053202_locale_order2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170505_053202_locale_order2 cannot be reverted.\n";

        return false;
    }
    */
}
