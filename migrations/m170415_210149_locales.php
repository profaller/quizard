<?php

use yii\db\Migration;

class m170415_210149_locales extends Migration
{
    public function safeUp()
    {
        $migration = new \yii\console\controllers\MigrateController('migrate', Yii::$app);
        $migration->runAction('up', ['migrationPath' => '@yii/i18n/migrations/', 'interactive' => false]);

        $this->createTable('languages', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string()->notNull()->unique(),
        ]);
    }

    public function safeDown()
    {
        echo "m170415_210149_locales cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170415_210149_locales cannot be reverted.\n";

        return false;
    }
    */
}
