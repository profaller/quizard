<?php

use app\models\Languages;
use yii\db\Migration;

class m170420_180045_base_langs extends Migration
{
    public function safeUp()
    {
        (new Languages([
            'name' => 'Russian',
            'code' => 'ru'
        ]))->save(false);

        (new Languages([
            'name' => 'English',
            'code' => 'en'
        ]))->save(false);
    }

    public function safeDown()
    {
        echo "m170420_180045_base_langs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170420_180045_base_langs cannot be reverted.\n";

        return false;
    }
    */
}
