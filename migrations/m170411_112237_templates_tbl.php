<?php

use yii\db\Migration;

class m170411_112237_templates_tbl extends Migration
{
    public function safeUp()
    {
        $this->createTable('templates', [
            'id' => $this->primaryKey(),
            'sid' => $this->string(32)->unique(),
            'name' => $this->string()->notNull()
        ]);
    }

    public function safeDown()
    {
        echo "m170411_112237_templates_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170411_112237_templates_tbl cannot be reverted.\n";

        return false;
    }
    */
}
