<?php

use yii\db\Migration;

class m170415_213050_locales2 extends Migration
{
    public function safeUp()
    {
        $this->createTable('locales', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string()->notNull()->unique(),
        ]);
    }

    public function safeDown()
    {
        echo "m170415_213050_locales2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170415_213050_locales2 cannot be reverted.\n";

        return false;
    }
    */
}
