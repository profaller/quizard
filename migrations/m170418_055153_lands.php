<?php

use yii\db\Migration;

class m170418_055153_lands extends Migration
{
    public function safeUp()
    {
        $this->createTable('landings', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'template_id' => $this->integer()->notNull(),
            'locale_id' => $this->integer()->notNull(),
            //'steps' => $this->text()
        ]);
    }

    public function safeDown()
    {
        echo "m170418_055153_lands cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170418_055153_lands cannot be reverted.\n";

        return false;
    }
    */
}
