<?php

use app\models\Locales;
use app\models\SourceMessage;
use yii\db\Migration;

class m170505_045015_locale_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn(SourceMessage::tableName(), 'order', $this->integer()->notNull()->defaultValue(500));

    }

    public function safeDown()
    {
        echo "m170505_045015_locale_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170505_045015_locale_order cannot be reverted.\n";

        return false;
    }
    */
}
