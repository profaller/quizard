<?php

use yii\db\Migration;

class m170418_211613_lands_steps extends Migration
{
    public function safeUp()
    {
        $this->createTable('landings_steps', [
            'landing_id' => $this->integer()->notNull(),
            'step_id' => $this->integer()->notNull(),
            'order' => $this->integer()->notNull()->defaultValue(500),
        ]);

        $this->addForeignKey('land_steps_to_land_ref', 'landings_steps', 'landing_id', 'landings', 'id', 'CASCADE');
        $this->addForeignKey('land_steps_to_steps_ref', 'landings_steps', 'step_id', 'steps', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170418_211613_lands_steps cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170418_211613_lands_steps cannot be reverted.\n";

        return false;
    }
    */
}
