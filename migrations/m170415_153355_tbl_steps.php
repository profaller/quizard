<?php

use yii\db\Migration;

class m170415_153355_tbl_steps extends Migration
{
    public function safeUp()
    {
        $this->createTable('steps', [
            'id' => $this->primaryKey(),
            'sid' => $this->string(32)->unique(),
            'name' => $this->string()->notNull(),
            'is_game' => $this->boolean()->notNull()->defaultValue(false)
        ]);
    }

    public function safeDown()
    {
        echo "m170415_153355_tbl_steps cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170415_153355_tbl_steps cannot be reverted.\n";

        return false;
    }
    */
}
