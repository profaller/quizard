<?php

namespace app\controllers;

class CloudflareController extends \app\components\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionResetCache()
    {
        $res = \Yii::$app->cloudflare->purgeCache();

        \Yii::$app->session->setFlash('cf', 'Cloudflare cache successfully resets');

        return $this->redirect(['index']);
    }

}
