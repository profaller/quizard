<?php

namespace app\controllers;

use app\components\Land;
use app\models\Images;
use app\models\Steps;
use app\models\TextPage;
use Yii;
use app\models\Landings;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LandingsController implements the CRUD actions for Landings model.
 */
class LandingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'allow' => true,
                'roles' => ['admin'],
            ]
        ];

        return $behaviors;
    }

    /**
     * Lists all Landings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Landings::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTextPage($id, $langCode, $code)
    {
        $land = new Land([
            'landingModel' => $this->findModel($id),
            'textPageModel' => TextPage::findOne(['code' => $code]),
            'langCode' => $langCode
        ]);

        $this->layout = 'landing-view';

        return $this->render('text-page', compact('land'));
    }

    public function actionView($id, $langCode, $imgCode, $stepId = false)
    {
        $model = $this->findModel($id);
        $imageModel = Images::findOne(['code' => $imgCode]);

        $steps = ArrayHelper::map($model->steps, 'id', function ($m) {return $m;});
        if ($stepId && isset($steps[$stepId])) {
            $stepModel = $steps[$stepId];
        } else {
            $stepModel = reset($steps);
        }


        if (!$model || !$stepModel || !$imageModel) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $land = new Land([
            'landingModel' => $model,
            'stepModel' => $stepModel,
            'imageModel' => $imageModel,
            'langCode' => $langCode,
        ]);

        $this->layout = 'landing-view';

        return $this->render('view', compact('land'));
    }

    /**
     * Creates a new Landings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Landings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Landings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Landings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Landings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Landings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Landings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
