<?php

namespace app\controllers;

use app\models\Languages;
use app\models\Message;
use app\models\SourceMessage;
use Yii;
use app\models\Locales;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * LocalesController implements the CRUD actions for Locales model.
 */
class LocalesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'allow' => true,
                'roles' => ['admin'],
            ]
        ];

        return $behaviors;
    }

    /**
     * Lists all Locales models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Locales::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Locales model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Locales();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Locales model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $exists = $model->sourceMessages;
            if (count($exists)) {
                SourceMessage::loadMultiple($exists, Yii::$app->request->post());
                if (SourceMessage::validateMultiple($exists)) {
                    foreach ($exists as $m) {
                        $m->save(false);
                    }
                }

                $messagesData = Yii::$app->request->post('Message', []);

                foreach ($exists as $srcModel) {

                    foreach ($messagesData as $code => $value) {
                        list($srcId, $langCode) = explode('_', $code);

                        if ($srcId != $srcModel->id) continue;

                        $messModel = Message::findOne(['id' => $srcId, 'language' => $langCode]);
                        if (!$messModel) {
                            $messModel = new Message();
                            $messModel->id = $srcModel->id;
                            $messModel->language = $langCode;
                        }
                        $messModel->translation = $value;
                        $messModel->save(true);

                        unset($messagesData[$code]);
                    }

                }
            }



            $srcModels = [];
            foreach (Yii::$app->request->post('newSourceMessage', []) as $i => $data) {
                $newModel = new SourceMessage();
                $newModel->attributes = $data;
                $newModel->category = (string) $model->id;
                if ($newModel->save()) {
                    $srcModels[$i] = $newModel;
                }
            }


            $messagesData = Yii::$app->request->post('newMessage', []);
            foreach ($srcModels as $index => $srcModel) {

                foreach ($messagesData as $code => $value) {
                    list($srcIndex, $langCode) = explode('_', $code);

                    if ($srcIndex != $index) continue;

                    $messModel = new Message();
                    $messModel->id = $srcModel->id;
                    $messModel->language = $langCode;
                    $messModel->translation = $value;
                    $messModel->save(true);

                    unset($messagesData[$code]);
                }
            }

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionImport($id)
    {
        $model = $this->findModel($id);

        $file = UploadedFile::getInstanceByName('import_file');

        if ($file) {

            $csv = array_map('str_getcsv', file($file->tempName));

            // langs
            foreach ($csv[0] as $i => $langCode) {
                if ($i == 0) continue;

                $lModel = Languages::findOne(['code' => $langCode]);
                if (!$lModel) {
                    $lModel = new Languages(['code' => $langCode, 'name' => $langCode]);
                    $lModel->save();
                }
            }

            array_walk($csv, function(&$a) use ($csv) {
                $a = array_combine($csv[0], $a);
            });
            array_shift($csv);

            $result = [];
            foreach ($csv as $item) {
                $key = $item['var_name'];
                array_shift($item);
                $result[$key] = $item;
            }


            SourceMessage::deleteAll(['category' => $id]);

            $i = 1;
            foreach ($result as $ph => $messages) {

                $src = new SourceMessage([
                    'category' => $id,
                    'message' => $ph,
                    'order' => $i++
                ]);

                if ($src->save()) {
                    foreach ($messages as $lang => $str) {
                        $message = new Message([
                            'id' => $src->id,
                            'language' => $lang,
                            'translation' => $str,
                        ]);
                        $message->save(false);
                    }
                }

            }
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    public function actionDeleteSrc($id)
    {
        $model = SourceMessage::findOne(['id' => $id]);

        if ($model) {
            $model->delete();
        }

        return $this->asJson([]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        SourceMessage::deleteAll(['category' => $model->id]);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Locales model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Locales the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Locales::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
